﻿namespace Shared.Dtos.Product;

public class UpdateProductDto
{
    public string Name { get; set; }

    public string Discription { get; set; }

    public string Sumary { get; set; }

    public decimal Price { get; set; }
}