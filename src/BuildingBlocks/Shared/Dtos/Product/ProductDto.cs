﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Shared.Dtos.Product;

public class ProductDto
{
    public long Id { get; set; }
    public string No { get; set; }

    public string Name { get; set; }

    public string Discription { get; set; }

    public string Sumary { get; set; }

    public decimal Price { get; set; }
}