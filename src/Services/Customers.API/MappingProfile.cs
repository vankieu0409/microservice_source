using AutoMapper;

using Shared.Dtos.Customers;

namespace Customers.API;

public class MappingProfile : Profile
{
    public MappingProfile()
    {
        CreateMap<Entities.Customers, CustomersDto>();
    }
}