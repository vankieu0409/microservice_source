namespace Customers.API.Services.Interfaces;

public interface ICustomerservice
{
    Task<IResult> GetCustomersByUsernameAsync(string username);
}