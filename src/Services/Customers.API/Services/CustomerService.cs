using AutoMapper;

using Customers.API.Repositories.Interfaces;
using Customers.API.Services.Interfaces;

using Shared.Dtos.Customers;

namespace Customers.API.Services;

public class Customerservice : ICustomerservice
{
    private readonly ICustomerRepository _repository;
    private readonly IMapper _mapper;

    public Customerservice(ICustomerRepository repository, IMapper mapper)
    {
        _repository = repository;
        _mapper = mapper;
    }

    public async Task<IResult> GetCustomersByUsernameAsync(string username)
    {
        var entity = await _repository.GetCustomerByUserNameAsync(username);
        var result = _mapper.Map<CustomersDto>(entity);

        return Results.Ok(result);
    }
}