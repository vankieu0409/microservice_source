using Contracts.Domains.Interfaces;

using Customers.API.Persistence;

namespace Customers.API.Repositories.Interfaces;

public interface ICustomerRepository : IRepositoryQueryBase<Entities.Customers, int, CustomerContext>
{
    Task<Entities.Customers> GetCustomerByUserNameAsync(string username);
}