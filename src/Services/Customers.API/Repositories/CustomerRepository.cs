using Customers.API.Persistence;
using Customers.API.Repositories.Interfaces;

using Infrastructure.Common;

using Microsoft.EntityFrameworkCore;

namespace Customers.API.Repositories;

public class CustomersRepository : RepositoryQueryBase<Entities.Customers, int, CustomerContext>, ICustomerRepository
{
    public CustomersRepository(CustomerContext dbContext) : base(dbContext)
    {

    }

    public Task<Entities.Customers> GetCustomerByUserNameAsync(string username) =>
        FindByCondition(x => x.UserName.Equals(username))
            .SingleOrDefaultAsync();
}