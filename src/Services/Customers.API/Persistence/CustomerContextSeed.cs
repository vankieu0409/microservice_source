﻿using Customers.API.Entities;
using Microsoft.EntityFrameworkCore;

namespace Customers.API.Persistence;

public static class CustomerContextSeed
{
    public static  IHost SeedCustomerData(this IHost host)
    {
        using (var scope = host.Services.CreateScope())
        {
            var customerContext = scope.ServiceProvider.GetRequiredService<CustomerContext>();
            customerContext.Database.MigrateAsync().GetAwaiter().GetResult();
            CreateCustomer(customerContext, "kieu", "Nguyen", "Van", "vankieu4009@gmail.com").GetAwaiter().GetResult();

        }
        return host;
    }

    private static async Task CreateCustomer(CustomerContext customerContext, string useName, string firstName,
        string lastName, string email)                                                                                                                                
    {
        var checkCustomer = await customerContext.Customers.SingleOrDefaultAsync(x => x.EmailAddress.Equals(email));
        if (checkCustomer == null)
        {
            var newCustomer = new Entities.Customers
            {
                UserName = useName,
                FirstName = firstName,
                LastName = lastName,
                EmailAddress = email
            };
            await customerContext.Customers.AddAsync(newCustomer);
            await customerContext.SaveChangesAsync();
        }
    }
}