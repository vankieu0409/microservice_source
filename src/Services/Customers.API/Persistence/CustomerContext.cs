﻿using Customers.API.Entities;
using Microsoft.EntityFrameworkCore;

namespace Customers.API.Persistence;

public class CustomerContext :DbContext
{
    public CustomerContext(DbContextOptions<CustomerContext> options):base(options)
    {
    }

    public DbSet<Entities.Customers> Customers {get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
        modelBuilder.Entity<Customers>(entity=>
        {
            entity.HasIndex(p => p.UserName).IsUnique();
            entity.HasIndex(p => p.EmailAddress).IsUnique();
        });
    }
}