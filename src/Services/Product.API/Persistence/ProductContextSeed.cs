﻿using Product.API.Entities;
using ILogger=Serilog.ILogger;

namespace Product.API.Persistence;

public class ProductContextSeed
{
    public static async Task SeedProductAsync(ProductContext productContext, ILogger logger)
    {
        if (!productContext.Products.Any())
        {
                productContext.AddRange(GetDataProduct());
                await productContext.SaveChangesAsync();
                logger.Information("Seeded data for BD associated with context {DbContextName}",nameof(ProductContext));
        }
    }

    private static IEnumerable<CatalogProduct> GetDataProduct()
    {
        return new List<CatalogProduct>()
        {
            new CatalogProduct()
                { No = "Lotus", Name = "Esprit", Sumary = "hehe", Discription = "kaka", Price = (decimal)1779.49 }
        };
    }
}