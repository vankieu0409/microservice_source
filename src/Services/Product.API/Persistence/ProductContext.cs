﻿
using Contracts.Domains.Interfaces;

using Microsoft.EntityFrameworkCore;

using Product.API.Entities;

namespace Product.API.Persistence
{
    public class ProductContext : DbContext
    {
        public ProductContext(DbContextOptions<ProductContext> options) : base(options)
        {

        }

        public DbSet<CatalogProduct> Products { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<CatalogProduct>().ToTable("Product");
            modelBuilder.Entity<CatalogProduct>(entity => entity.HasKey(p => p.Id));
            modelBuilder.Entity<CatalogProduct>(entity => entity.HasIndex(p => p.No).IsUnique());

            //Take Now DateTime from Server and get value automatically
            //modelBuilder.Entity<CatalogProduct>().Property(b => b.LastModifiedDate).HasDefaultValueSql("CONVERT(datetime,GetDate())");
            //modelBuilder.Entity<CatalogProduct>().Property(b => b.CreatedDate).HasDefaultValueSql("CONVERT(datetime,GetDate())");
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellation = new CancellationToken())
        {
            var modified = ChangeTracker.Entries()
                .Where(e => e.State == EntityState.Modified ||
                            e.State == EntityState.Added ||
                            e.State == EntityState.Deleted);
            foreach (var item in modified)
            {
                switch (item.State)
                {
                    case EntityState.Added:
                        if (item.Entity is IDateTracking addedEntity)
                        {
                            addedEntity.CreatedDate = DateTime.UtcNow;
                            item.State = EntityState.Added;
                        }
                        break;
                    case EntityState.Modified:
                        Entry(item.Entity).Property("Id").IsModified = false;
                        if (item.Entity is IDateTracking modifiedEntity)
                        {
                            modifiedEntity.LastModifiedDate = DateTime.UtcNow;
                            item.State = EntityState.Modified;
                        }
                        break;
                }
            }
            return base.SaveChangesAsync(cancellation);
        }
    }
}
