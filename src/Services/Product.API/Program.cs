using Common.Logging;

using Product.API.Extensions;
using Product.API.Persistence;

using Serilog;

var builder = WebApplication.CreateBuilder(args);
Log.Information("Start up Product API");

// Add services to the container.
try
{
    builder.Host.UseSerilog(Serilogger.Configure);

    builder.Host.AddAppConfigurations();
    builder.Services.AddInfrastructure(builder.Configuration);

    var app = builder.Build();

    app.UserInfrastructure();
    app.MapControllers();
    app.MigrateDatabase<ProductContext>((context, _) =>
    {
        ProductContextSeed.SeedProductAsync(context, Log.Logger).Wait();
    }).Run();

}
catch (Exception ex)
{
    if (ex.GetType().Name.Equals("StopTheHostException", StringComparison.Ordinal)) throw;

    Log.Fatal(ex, "Unhandled Exception");
}
finally
{
    Log.Information("Shutting down Product API");
    Log.CloseAndFlush();
}