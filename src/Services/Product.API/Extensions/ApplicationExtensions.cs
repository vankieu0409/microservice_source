﻿using Microsoft.AspNetCore.Builder;

namespace Product.API.Extensions;

public static class ApplicationExtensions
{
    public static void UserInfrastructure(this IApplicationBuilder app)
    {
        // Configure the HTTP request pipeline.

        app.UseSwagger();
        app.UseSwaggerUI();


        app.UseHttpsRedirection();

        app.UseAuthorization();
        //app.UseEndpoints(endpoints =>
        //{
        //    endpoints.MapDefaultControllerRoute();
        //});
    }
}