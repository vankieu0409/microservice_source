﻿using Contracts.Domains.Interfaces;

using Infrastructure.Common;

using Microsoft.EntityFrameworkCore;

using MySqlConnector;

using Pomelo.EntityFrameworkCore.MySql.Infrastructure;

using Product.API.Persistence;
using Product.API.Repositories;

namespace Product.API.Extensions;

public static class ServiceExtensions
{
    public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddControllers();
        services.Configure<RouteOptions>(options => options.LowercaseUrls = true);
        services.AddEndpointsApiExplorer();
        services.AddSwaggerGen();
        services.ConfigureProductDbContext(configuration);
        services.AddInfrastructureService();
        services.AddAutoMapper(typeof(Program));
        return services;
    }

    private static IServiceCollection ConfigureProductDbContext(this IServiceCollection services, IConfiguration configuration)
    {
        var connectionString = configuration.GetConnectionString("MySQLConnection");
        var builder = new MySqlConnectionStringBuilder(connectionString);
        services.AddDbContext<ProductContext>(options => options.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString), e =>
        {
            e.MigrationsAssembly("Product.API");
            e.SchemaBehavior(MySqlSchemaBehavior.Ignore);
        }));
        return services;
    }
    private static IServiceCollection AddInfrastructureService(this IServiceCollection services)
    {
        return services.AddScoped(typeof(IRepositoryBase<,,>), typeof(RepositoryBaseAsync<,,>))
             .AddScoped(typeof(IUnitOfWork<>), typeof(UnitOfWork<>))
             .AddScoped<IProductRepository, ProductRepository>();

    }
}